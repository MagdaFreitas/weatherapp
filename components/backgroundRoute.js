const BackgroundImages = {

  '01d': require('../assets/backg/01d.jpg'),
  '01n': require('../assets/backg/01n.jpg'),
  '02d': require('../assets/backg/02d.jpeg'),
  '02n': require('../assets/backg/02n.jpeg'),
  '03d': require('../assets/backg/03d.jpeg'),
  '03n': require('../assets/backg/03n.jpeg'),
  '04d': require('../assets/backg/04d.jpeg'),
  '04n': require('../assets/backg/04n.jpeg'),
  '09n': require('../assets/backg/09n.jpeg'),
  '09d': require('../assets/backg/09d.jpeg'),
  '10d': require('../assets/backg/10d.jpeg'),
  '10n': require('../assets/backg/10n.jpeg'),
  '11d': require('../assets/backg/11d.jpeg'),
  '11n': require('../assets/backg/11n.jpg'),
  '13d': require('../assets/backg/13d.jpg'),
  '13n': require('../assets/backg/13n.jpg'),
  '50d': require('../assets/backg/50d.jpeg'),
  '50n': require('../assets/backg/50n.jpg')
}

export default BackgroundImages