import React from 'react';
import { Image, Text, ShadowPropTypesIOS } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

const api_key = 'AIzaSyB4v6HRtteNW_53ypNkwWKDVV9ED1tsoJA';


const GooglePlacesInput = (props) => {
  return (
    <GooglePlacesAutocomplete
      placeholder='Search'
      minLength={2} // minimum length of text to search
      autoFocus={false}
      returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
      keyboardAppearance={'light'} // Can be left out for default keyboardAppearance https://facebook.github.io/react-native/docs/textinput.html#keyboardappearance
      listViewDisplayed='auto'    // true/false/undefined
      fetchDetails={true}
      renderDescription={row => row.description} // custom description render

    onPress={ async (data, details=null) => { // 'details' is provided when fetchDetails = true
    console.log('data =====>',data);
    const resJson = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?place_id=${data.place_id}&key=${api_key}`)
    const response = await resJson.json()
    console.log('response =====>', response)
    props.getData(response.results[0].geometry.location.lat, response.results[0].geometry.location.lng)
    props.location()
}}

      getDefaultValue={() => ''}

      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: 'AIzaSyB4v6HRtteNW_53ypNkwWKDVV9ED1tsoJA',
        language: 'en', // language of the results
        types: '(cities)' // default: 'geocode'
      }}

      styles={{
        textInputContainer: {
          width: '85%',
          borderRadius:15,
          marginTop:10,
          marginLeft:5,
          backgroundColor:'#878c88'
        },
        description: {
          fontWeight: 'bold'
        },
        predefinedPlacesDescription: {
          color: 'black'
        }
      }}

      // currentLocation={true} 
      // currentLocationLabel="Current location"
      nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
      GoogleReverseGeocodingQuery={{
        // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
      }}
      GooglePlacesSearchQuery={{
        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
        rankby: 'distance',
        type: 'cafe'
      }}
      
      GooglePlacesDetailsQuery={{
        // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
        fields: 'formatted_address',
      }}

      filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities

      debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
      // renderLeftButton={()  => <Image source={require('path/custom/left-icon')} />}
      // renderRightButton={() => <Text>Custom text after the input</Text>}
    />
  );
}

export default GooglePlacesInput