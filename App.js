import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity, Modal, Alert, ScrollView, Linking } from 'react-native';
import moment from 'moment';
import IconImages from './components/imageRoute.js';
import BackgroundImages from './components/backgroundRoute.js';
import Location from './components/Location.js'

export default function App() {

  const [wState, setWState] = useState(null)
  const [loading, setLoading] = useState(true)
  const [forecastState, setForecastState] = useState(null)
  const [fact, setFact] = useState(null)
  const [showLoc, setShowLoc] = useState (false)
  const [showAbout, setShowAbout] = useState (false)

  let api_key = '16909a97489bed275d13dbdea4e01f59'

  useEffect(() => {

    console.log('wState: ', wState)
    console.log('forecastState: ', forecastState)
    let loc = navigator.geolocation.getCurrentPosition( coords =>
    getData(coords.coords.latitude, coords.coords.longitude))
      if (!loc){getData(51.51, -0.13)}
    getFact()
  }, [])

  useEffect(() => {
    if(wState !== null)setLoading(false)
  }, [wState])

  let location = () => {
    setShowLoc(!showLoc)
  }

  let about = () => {
    setShowAbout(!showAbout)
  }

  let getData = async (latitude, longitude) =>{

    try {
    const res1 = await fetch(`http://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&units=metric&appid=${api_key}`)
    const res2 = await res1.json()
    const res3 = await fetch(`http://api.openweathermap.org/data/2.5/forecast/daily?lat=${latitude}&lon=${longitude}&units=metric&appid=${api_key}`)
    const res4 = await res3.json()

    setForecastState(res4)
    setWState(res2)

    } catch (error) {
      console.log('getData error: ', error)
    }
  }

  const getFact  =  async () => {

    try {
      const response5= await fetch(`https://geek-jokes.sameerkumar.website/api`)
      const response6 = await response5.json()
      console.log ('RES',response6)
      setFact(response6)
    }
      catch(error)
    {
      console.log(error)
    }
}


  return (loading ? null :
    <View>

      <ImageBackground style={styles.background} source={BackgroundImages[wState.weather[0].icon]}>

    <TouchableOpacity style={styles.searchLoc} onPress={()=> location()}>
    <Image style={styles.searchI} source={require('./assets/icons/earthalone.png')}/>
    </TouchableOpacity>

    <TouchableOpacity style={styles.more} onPress={()=> about()}>
    <Image style={styles.moreI} source={require('./assets/icons/more.png')}/>
    </TouchableOpacity>

    <View style={styles.main}>
      <Text>{wState.name}</Text>
      <Text>{wState.sys.country}</Text>
      <Text>Current: {parseInt(wState.main.temp)}º</Text>
      <Image style={styles.mainI} source={IconImages[wState.weather[0].icon]}/>
      <Text>Max: {parseInt(wState.main.temp_max)}º Min: {parseInt(wState.main.temp_min)}º</Text>
    </View>

    <View style={styles.forecast}>
      <View style={styles.forecastView}>
      <Text>{moment(new Date(forecastState.list[1].dt*1000)).format('ddd')}</Text>
      <Text>{parseInt(forecastState.list[1].temp.max)}º</Text>
      <Image style={styles.forecastI} source={IconImages[forecastState.list[1].weather[0].icon]}/>
      <Text>{parseInt(forecastState.list[1].temp.min)}º</Text>
    </View>

    <View style={styles.forecastView}>
      <Text>{moment(new Date(forecastState.list[2].dt*1000)).format('ddd')}</Text>
      <Text>{parseInt(forecastState.list[2].temp.max)}º</Text>
      <Image style={styles.forecastI} source={IconImages[forecastState.list[2].weather[0].icon]}/>
      <Text>{parseInt(forecastState.list[2].temp.min)}º</Text>
    </View>

    <View style={styles.forecastView}>
      <Text>{moment(new Date(forecastState.list[3].dt*1000)).format('ddd')}</Text>
      <Text>{parseInt(forecastState.list[3].temp.max)}º</Text>
      <Image style={styles.forecastI} source={IconImages[forecastState.list[3].weather[0].icon]}/>
      <Text>{parseInt(forecastState.list[3].temp.min)}º</Text>
    </View>

    <View style={styles.forecastView}>
      <Text>{moment(new Date(forecastState.list[4].dt*1000)).format('ddd')}</Text>
      <Text>{parseInt(forecastState.list[4].temp.max)}º</Text>
      <Image style={styles.forecastI} source={IconImages[forecastState.list[4].weather[0].icon]}/>
      <Text>{parseInt(forecastState.list[4].temp.min)}º</Text>
    </View>

    <View style={styles.forecastView}>
      <Text>{moment(new Date(forecastState.list[5].dt*1000)).format('ddd')}</Text>
      <Text>{parseInt(forecastState.list[5].temp.max)}º</Text>
      <Image style={styles.forecastI} source={IconImages[forecastState.list[5].weather[0].icon]}/>
      <Text>{parseInt(forecastState.list[5].temp.min)}º</Text>
    </View>

    </View>

    <ScrollView>
      <View style={styles.facts}>
      <Text style={{textAlign:'center', marginBottom:5}}>Did you know?</Text>
      <Text>{fact}</Text>
      </View>
    </ScrollView>

    <Modal
      animationType="slide"
      transparent={true}
      visible={showLoc}
      onRequestClose={() => {
      Alert.alert("Modal has been closed.");
      }}>

    <View style={styles.locationContainer}>
      <Location style = {styles.location} location={location} getData={getData}/>

      <TouchableOpacity onPress={()=> location()} style={styles.locationA}>
      <Image source={require('./assets/icons/arrow.png')} style={styles.locationArr} />
     </TouchableOpacity>
    </View>
      </Modal>

    <Modal
      animationType="fade"
      transparent={true}
      visible={showAbout}
      onRequestClose={() => {
      Alert.alert("Modal has been closed."); }}>

    <View style={styles.aboutContainer}>
        <Text style={styles.aboutText}>
          About:
        </Text>
        <Text style={styles.aboutText}>
        Created by Magda Freitas
        </Text>
        <Text style={styles.aboutText}>
        Icons made by Freepik from www.flaticon.com
        </Text>
        <Text onPress={() => Linking.openURL('http://mysterious-prose.surge.sh/')} style={styles.moreText}>Tap this for more projects.
        </Text>
        
        <TouchableOpacity onPress={()=> about()} style={styles.locationA}>
          <Image source={require('./assets/icons/arrow.png')} style={styles.locationArr} />
        </TouchableOpacity>

          <Image source={require('./assets/icons/favourite.png')} style={styles.heart} />
    </View>
    </Modal>

    </ImageBackground>
    
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  searchI: {
    width:55,
    height:55,
  },

  searchLoc: {
    marginTop: '10%',
    marginLeft: '80%',
  },

  more: {
    position:"absolute",
    top: '5.5%',
    right: '85%',
  },

  moreI: {
    width:36,
    height:36,
  },

  aboutText: {
    position:'relative',
    top:50,
    left:50
  },

  moreText: {
    position:'relative',
    fontWeight:'bold',
    top:50,
    left:50
  },


  locationContainer: {
    flex: 1,
    width: '100%',
    backgroundColor: 'white',
    alignItems: 'flex-start',
    borderWidth:1,
    borderRadius:15,
    marginTop:150,
    backgroundColor: 'rgba(255,255,255,0.9)'
  },

  aboutContainer: {
    flex: 1,
    width: '100%',
    backgroundColor: 'white',
    alignItems: 'flex-start',
    borderWidth:1,
    borderRadius:15,
    marginTop:500,
    backgroundColor: 'rgba(255,255,255,1)',
  },

  locationA: {
  position:'absolute',
  top:'3%',
  left:'88%'
  },

  heart: {
  width:50,
  height:50,
  position:'absolute',
  top:'68%',
  left:'85%'
  },

  main: {
    backgroundColor: 'rgba(255,255,255,0.5)',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:10,
    marginBottom:50,
    borderWidth:1,
    borderRadius:35,
    marginRight:70,
    marginLeft:70,
    padding: 15,
  },

  mainI: {
    width:150,
    height:150,
    marginTop:'5%',
    marginBottom:'5%'
  },

  forecast: {
    flexDirection:'row',
    justifyContent:'space-evenly',
  },

  forecastView: {
    borderWidth:1,
    borderRadius:10,
    backgroundColor: 'rgba(255,255,255,0.5)',
    padding:10
  },


  forecastI: {
    width:35,
    height:35,
    marginTop:'5%',
    marginBottom:'5%',
  },

  background: {
    width:'100%',
    height:'100%'
  },

  facts: {
    textAlign:'center',
    color: 'grey',
    marginTop:'10%',
    marginLeft:'3%',
    marginRight:'3%',
    marginBottom:'3%',
    backgroundColor: 'rgba(255,255,255,0.5)',
    borderWidth:1,
    borderRadius:10,
    backgroundColor: 'rgba(255,255,255,0.5)',
    padding:10,
  }
});